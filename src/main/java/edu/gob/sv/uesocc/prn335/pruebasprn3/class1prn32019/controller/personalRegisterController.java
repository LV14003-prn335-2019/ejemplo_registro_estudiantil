/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.controller;

import edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.models.Student;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import java.util.List;
import java.util.ListIterator;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author cesarlinares
 */
@Stateless
@LocalBean
public class personalRegisterController implements Serializable {

    @Inject
    private Student student;

    public static List<Student> StudentList = new ArrayList<>();
/***
 * Add Student to arraylist of stundents
 * @param student dummy model
 * @return 
 */
    public String Create(Student student) {

        if (student != null) {

            StudentList.add(student);
            return "Register Created success";

        }
        return "Register does not created";

    }
    
    /***
     * Retunr list from all students tha exist 
     * @return 
     */
    
    public List<Student> GetAllStudent() {
       
        return StudentList;
    }
    
    

    public String CreateRegister(final String name) {
        if (name != null) {
            return name;
        }
        return "Error : add infromation";

    }

}
