/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.boundary.servelet;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.formatDate;
import edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.controller.personalRegisterController;
import edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.models.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sun.font.EAttribute;
import sun.java2d.pipe.SpanShapeRenderer;

/**
 *
 * @author cesarlinares
 */
@WebServlet(name = "PersonalRegister", urlPatterns = {"/PersonalRegister"})
public class PersonalRegister extends HttpServlet {

    @Inject
    private personalRegisterController PersonalRegisterCtrl;

    @Inject
    private Student student;
    static List<Student> listaSt;


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            /**
             * *
             * dynimic code for application page
             */
            student.setName(request.getParameter("name"));
            student.setEmail(request.getParameter("email"));
            student.setPsw(request.getParameter("psw"));
            student.setCareer(request.getParameter("career"));
            student.setBirth(request.getParameter("birth"));

            out.println("<!DOCTYPE html>");
            out.println("<html>");

            out.println("<head>");
            out.println("<title>Servlet PersonalRegister</title>");

            out.println("<link href=./css/site.css rel=stylesheet type=text/css>");

            out.println("</head>");

            out.println("<body>");
            out.println("<h1 style='text-align:center;'>Register Created Succees  " + PersonalRegisterCtrl.Create(student) + "</h1>");

            /**
             * *
             * Draw table using foreach
             */
           listaSt = PersonalRegisterCtrl.GetAllStudent();

            out.println("<table id='customers'");
            out.println("<tr>");
            out.println("<th> Student Name</th>");
            out.println("<th> Email </th>");
            out.println("<th> Career</th>");
            out.println("<th> Birthday</th>");
            out.println("</tr>");
            listaSt.forEach((Student item) -> {
                out.println("<tr>");
                out.println("<td>  " + item.getName() + "</td>");
                out.println("<td> " + item.getEmail() + "</td>");
                out.println("<td> " + item.getCareer() + "</td>");
                out.println("<td> " + item.getBirth() + "</td>");
                out.println("</tr>");
            });
            out.println("</table>");

            out.println("<br><form action=\"index.html\"><input type=\"submit\" value=\"REGRESAR AL INDEX\" /></form>");

            out.println("</body>");
            out.println("</html>");

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
