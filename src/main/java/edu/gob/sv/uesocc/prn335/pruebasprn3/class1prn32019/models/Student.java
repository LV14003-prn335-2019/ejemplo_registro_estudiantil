/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.gob.sv.uesocc.prn335.pruebasprn3.class1prn32019.models;

import java.io.Serializable;
import javax.ejb.Stateless;

import javax.persistence.Entity;
import javax.persistence.Id;
import sun.util.calendar.BaseCalendar.Date;

/**
 *
 * @author cesarlinares
 */
@Entity
@Stateless
public class Student implements Serializable {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getPsw() {
        return Psw;
    }

    public void setPsw(String Psw) {
        this.Psw = Psw;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }
    @Id
    private long Id;

    public long getId() {
        return Id;
    }

    public void setId(long Id) {
        this.Id = Id;
    }
    private String name;
    private String career;
    private String Email;
    private String Psw;
    private String birth;

}
